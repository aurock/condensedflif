var monstersNumber = 2;

var monsters = {
    monster1 : {
        name : "rat",
        lvl : 1,
        exp : 1,
        hpBase : 10,
        hp : 10,
        atk : 1,
        atkSpeed : 2000
    },
    
    monster2 : {
        name : "goblin",
        lvl : 2,
        exp : 2,
        hpBase : 15,
        hp : 15,
        atk : 2,
        atkSpeed : 2000
    },

    monster3 : {
        name : "thief",
        lvl : 3,
        exp : 3,
        hpBase : 22,
        hp : 22,
        atk : 4,
        atkSpeed : 2000
    }
};


